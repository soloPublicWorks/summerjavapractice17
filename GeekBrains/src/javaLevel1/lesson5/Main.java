package javaLevel1.lesson5;

/**
 * Created by Валера on 13.07.2017.
 */
public class Main {
    public static void main(String[] args) {
        Employee[] employees = new Employee[5];
        employees[0] = new Employee("Иван", "Сидоров", "Иванович", 20);
        employees[1] = new Employee("Вася", "Петров", "Петрович", 51);
        employees[2] = new Employee("Коля", "Иванов", "Сидорович", 18);
        employees[3] = new Employee("Даша", "Соломинская", "Ивановна", 40);
        employees[4] = new Employee("Маша", "Лурк", "Ивановна", 41);

        for (Employee employee : employees)
            if (employee.getAge() > 40) System.out.println(employee);
    }
}

class Employee {
    private static String unknownFieldStr = "unknown";
    private static int unknownFieldStrInt = 0;

    private String name = unknownFieldStr;
    private String surname = unknownFieldStr;
    private String middleName = unknownFieldStr;
    private String post = unknownFieldStr;
    private String phoneNumber = unknownFieldStr;
    private String email = unknownFieldStr;
    private double salary = unknownFieldStrInt;
    private int age = unknownFieldStrInt;

    // данные относящиеся напрямую к человеку
    public Employee(String name, String surname, String middleName, int age) {
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.age = age;
    }

    public Employee(String name, String surname, String middleName, int age,
                    String post, double salary,
                    String phoneNumber, String email) {
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.post = post;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.salary = salary;
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Employee {" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", middleName='" + middleName + '\'' +
                ", post='" + post + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", salary=" + salary +
                ", age=" + age +
                '}';
    }
}

package javaLevel1.lesson4;

/**
 * Created by Валера on 08.07.2017.
 */
public class Stroke {
    private int x;
    private int y;

    public Stroke(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}

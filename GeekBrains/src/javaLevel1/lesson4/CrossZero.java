package javaLevel1.lesson4;

import javaLevel1.lesson4.Stroke;
import jdk.nashorn.internal.ir.annotations.Ignore;

import java.util.*;
public class CrossZero {
    private static char[][] field;
    private static final int SIZE = 3;
    private static final char DOT_EMPTY = '*';
    private static final char DOT_CROSS = 'X';
    private static final char DOT_ZERO = 'O';
    private static final int LEVELS_NUMBER = 3;
    private static byte level;
    private static char playerSide;
    private static char computerSide;
    private static Deque<Stroke> playerStrokesJournal = new ArrayDeque<>();
    private static Deque<Stroke> computerStrokesJournal = new ArrayDeque<>();
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        introduction();
        play();
    }

    private static void introduction() {
        System.out.println("Крестики-нолики");
        initField();
        printFiled();
    }
    private static void play() {
        boolean gameEnded = false;
        turnLevel();
        playerSide = turnSide();
        computerSide = (playerSide == DOT_CROSS) ? DOT_ZERO : DOT_CROSS;
        int movesCounter;
        printBorder();

        // выбор игрока идущего первым
        if (playerSide == DOT_CROSS) movesCounter = 0;
        else movesCounter = 1;

        // процесс игры
        while (!gameEnded) {
            if (movesCounter % 2 == 0) gameEnded = playerTurn();
            else gameEnded = computerTurn();
            printBorder();

            if (gameEnded) {
                if (movesCounter % 2 == 0) System.out.println("Победил игрок!");
                else System.out.println("Победил компьютер!");
            }

            if (!gameEnded && isDraw()) {
                gameEnded = true;
                System.out.println("Ничья!");
            }
            movesCounter++; // переход хода
        }
    }

    private static void turnLevel() {
        boolean isRightInput = false;
        do {
            System.out.print("Выберите уровень сложности (от 0 до " + (LEVELS_NUMBER - 1) + "): ");
            if (scanner.hasNextByte()) {
                level = scanner.nextByte();
                if (isLevelValid(level)) isRightInput = true;
            } else printInputErrorMessage();
        } while (!isRightInput);
    }
    private static char turnSide() {
        boolean isRightInput = false;
        String sideString;
        char side = 0;
        do {
            System.out.print("Выберите, чем хотите играть(англ.X/англ.O): ");
            if (scanner.hasNext()) {
                sideString = scanner.next();
                if (isSideValid(sideString)) {
                    side = sideString.charAt(0);
                    isRightInput = true;
                }
            } else scanner.nextLine();
        } while (!isRightInput);
        return side;
    }

//-----------------------------------------------Методы поля----------------------------------------------------------//
    private static void initField() {
        field = new char[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++)
            Arrays.fill(field[i], DOT_EMPTY);
    }
    private static void printFiled() {
        for (int i = 0; i <= SIZE; i++) System.out.print(i+ "  ");
        System.out.println();
        for (int i = 0; i < SIZE; i++)
            System.out.println(i + 1 + " " + Arrays.toString(field[i]));
    }

//-----------------------------------------------Методы ходов---------------------------------------------------------//
    // после каждого хода печатается поле игры
    // проверка на выйграшность хода - внутри хода
    // возращает boolean - является ли ход выигрышным
    private static boolean playerTurn() {
        System.out.println("Ход игрока:");  scanner.nextLine();
        byte x = 0, y = 0;
        boolean isRightInput = false;
        do {
            System.out.print("Введите две координаты: ");
            if (scanner.hasNextByte()) x = (byte) (scanner.nextByte() - 1);
            if (scanner.hasNextByte()) y = (byte) (scanner.nextByte() - 1);
            if (isCellValid(x, y, true)) {
                setMark(playerSide, x, y);
                isRightInput = true;
            }
        } while (!isRightInput);
        return checkWin(playerSide, x, y);
    }
    private static boolean computerTurn() {
        System.out.println("Ход компьютера:");
        boolean computerWon = false;
        switch (level) {
            case 0:
                computerWon = computerTurnEasyLevel();
                break;
            case 1:
                computerWon = computerTurnMiddleLevel();
                break;
            case 2:
                computerWon = computerTurnHardLevel();
                break;
        }
        return computerWon;
    }
    private static boolean computerTurnEasyLevel() {
        byte x, y;
        boolean isRightInput = false;
        do {
            x = (byte) ((byte) (1 + Math.random()*SIZE) - 1);
            y = (byte) ((byte) (1 + Math.random()*SIZE) - 1);
            if (isCellValid(x, y, false)) {
                setMark(computerSide, x, y);
                isRightInput = true;
            }
        } while (!isRightInput);
        return checkWin(computerSide, x, y);
    }
    private static boolean computerTurnMiddleLevel() {
        return doNeatGame();
    }

    @Ignore // недоделано
    private static boolean computerTurnHardLevel() {
        char playerSide = computerSide == DOT_CROSS ? DOT_ZERO : DOT_CROSS;
        if (computerSide == DOT_CROSS) {    // ходит первым
            if (field[SIZE/2][SIZE/2] == DOT_EMPTY) {
                setMark(computerSide, SIZE/2, SIZE/2);   // занять центр
                return checkWin(computerSide,
                        computerStrokesJournal.getLast().getX(),
                        computerStrokesJournal.getLast().getY());
            }
            else {
                //  противник в углу
                if (enemyIsInCorner(playerSide)) return doNeatGame();
                // если противник не занял угловую позицию
                else return enemyDidNotStayInCorner();
            }
        } else   // ходит вторым
            return doNeatGame();
    }

    private static void setMark(char side, int x, int y) {
        field[x][y] = side;
        if (side == playerSide) playerStrokesJournal.addLast(new Stroke(x, y));
        else computerStrokesJournal.addLast(new Stroke(x, y));
        printFiled();
    }

//-----------------------------------------------Проверки-------------------------------------------------------------//
    // проверка только тех линий, на пересечении которых игрок поставил пометку
    // быстрее полной проверки поля: O(4n) < O(n^2)

    // победит ли dotType, если есть пометка в (x_lastTurn, y_lastTurn)
    private static boolean checkWin(char dotType, int x_lastTurn, int y_lastTurn) {
        boolean lastTurnIsFake = false;
        // установить маркер для проверки
        if (field[x_lastTurn][y_lastTurn] == DOT_EMPTY) {
            field[x_lastTurn][y_lastTurn] = dotType;
            lastTurnIsFake = true;
        }
        boolean isWon = false;
        if (x_lastTurn == y_lastTurn) isWon = checkMainDiagonal(dotType);
        if (y_lastTurn == SIZE - 1 - x_lastTurn && !isWon) isWon = checkSideDiagonal(dotType);
        if (!isWon) isWon = checkDotVerticalAndHorizontal(dotType, x_lastTurn, y_lastTurn);

        // снять маркер после проверки
        if (lastTurnIsFake) field[x_lastTurn][y_lastTurn] = DOT_EMPTY;

        return isWon;
    }
    private static boolean checkDotVerticalAndHorizontal(char dotType, int x_lastTurn, int y_lastTurn) {
        boolean isWon = checkDotHorizontal(dotType, x_lastTurn);
        if (!isWon) isWon = checkDotVertical(dotType, y_lastTurn);
        return isWon;
    }
    private static boolean checkDotVertical(char dotType, int y) {
        boolean isWon = true;
        for (int i = 0; i < SIZE && isWon; i++)
            if (field[i][y] != dotType) isWon = false;
        return isWon;
    }
    private static boolean checkDotHorizontal(char dotType, int x) {
        boolean isWon = true;
        for (int i = 0; i < SIZE && isWon; i++)
            if (field[x][i] != dotType) isWon = false;
        return isWon;
    }
    private static boolean checkMainDiagonal(char dotType) {
        boolean isWon = true;
        for (int i = 0; i < SIZE && isWon; i++)
            if (field[i][i] != dotType) isWon = false;
        return isWon;
    }
    private static boolean checkSideDiagonal(char dotType) {
        boolean isWon = true;
        for (int i = 0; i < SIZE && isWon; i++)
            if (field[i][SIZE - 1 - i] != dotType) isWon = false;
        return isWon;
    }

    // ничья
    private static boolean isDraw() {
        for (int i = 0; i < SIZE; i++)
            for (int j = 0; j < SIZE; j++)
                if (field[i][j] == DOT_EMPTY) return false;
        return true;
    }

    // правильность ввода
    private static boolean isCellValid(int x, int y, boolean needMessageAboutWrongInput) {
        if (x < 0 || x > SIZE - 1 || y < 0 || y > SIZE - 1) {
            if (needMessageAboutWrongInput) printInputErrorMessage();
            return false;
        } else if (field[x][y] != DOT_EMPTY) {
            if (needMessageAboutWrongInput) printCellUnavailableMessage();
            return false;
        } else return true;
    }
    private static boolean isSideValid(String side) {
        if (side.length() == 1 && (side.charAt(0) == DOT_CROSS || side.charAt(0) == DOT_ZERO)) return true;
        else {
            printInputErrorMessage();
            return false;
        }
    }
    private static boolean isLevelValid(int level) {
        if (level >= 0 && level < LEVELS_NUMBER) return true;
        else {
            printInputErrorMessage();
            return false;
        }
    }

//-----------------------------------------------Вспомогательные методы-----------------------------------------------//
    private static void printBorder() {
        System.out.println("------------------------");
    }
    private static void printInputErrorMessage() {
        System.out.println("Неверный ввод!");
        scanner.nextLine();
    }
    private static void printCellUnavailableMessage() {
        System.out.println("Клетка занята.");
        scanner.nextLine();
    }

//-----------------------------------------------Искуственный интеллект-----------------------------------------------//
    private static boolean enemyIsInCorner(char enemySide) {
        boolean isInCorner = false;
        if (field[0][0] == enemySide) isInCorner = true;
        else if (field[0][SIZE - 1] == enemySide) isInCorner = true;
        else if (field[SIZE - 1][0] == enemySide) isInCorner = true;
        else if (field[SIZE - 1][SIZE - 1] == enemySide) isInCorner = true;
        return isInCorner;
    }
    // возвращает выигрышную позицию. если такой нет, то вернет null
    private static Stroke getWinStroke() {
        for (int x = 0; x < SIZE; x++)
            for (int y = 0; y < SIZE; y++)
                if (checkWin(computerSide, x, y)) return new Stroke(x, y);
        return null;
    }
    // возвращает позицию блокировки победы противника. если такой нет, то вернет null
    private static Stroke getBlockStroke() {
        for (int x = 0; x < SIZE; x++)
            for (int y = 0; y < SIZE; y++)
                if (checkWin(playerSide, x, y)) return new Stroke(x, y);
        return null;
    }
    // стратегии
    private static boolean enemyDidNotStayInCorner() {
        int x = 0, y = 0;
        Stroke lastMoveOfPlayer = playerStrokesJournal.getLast();
        switch (lastMoveOfPlayer.getX()) {
            case 0:
                x = lastMoveOfPlayer.getX();
                y = lastMoveOfPlayer.getY() - 1;
                setMark(computerSide, x,y);
                break;
            case 1:
                if (lastMoveOfPlayer.getY() == 1) {
                    x = lastMoveOfPlayer.getX() - 1;
                    y = lastMoveOfPlayer.getY();
                    setMark(computerSide, x, y);
                } else {
                    x = lastMoveOfPlayer.getX() + 1;
                    y = lastMoveOfPlayer.getY();
                    setMark(computerSide, x, y);
                }
                break;
            case 2:
                x = lastMoveOfPlayer.getX();
                y = lastMoveOfPlayer.getY() + 1;
                setMark(computerSide, x, y);
        }
        Stroke winStroke = getWinStroke();
        if (winStroke != null) setMark(computerSide, winStroke.getX(), winStroke.getY());
        else {
            Stroke lastMoveOfComputer = computerStrokesJournal.getLast();
            switch (lastMoveOfComputer.getX()) {
                case 0:
                    if (playerStrokesJournal.getFirst().getX() == 0) {
                        x = SIZE - 1;
                        y = playerStrokesJournal.getFirst().getY() - 1;
                        setMark(computerSide, x, y);
                    } else {
                        x = playerStrokesJournal.getFirst().getX() - 1;
                        y = SIZE - 1;
                        setMark(computerSide, x, y);
                    }
                    break;
                case 2:
                    if (playerStrokesJournal.getFirst().getX() == SIZE - 1) {
                        x = 0;
                        y = playerStrokesJournal.getFirst().getY() + 1;
                        setMark(computerSide, x, y);
                    } else {
                        x = playerStrokesJournal.getFirst().getX() + 1;
                        y = 0;
                        setMark(computerSide, x, y);
                    }
                    break;
            }
        }
        return checkWin(computerSide, x, y);
    }
    private static boolean doNeatGame() {
        Stroke winStroke = getWinStroke();
        if (winStroke != null) {    // если есть победный ход
            setMark(computerSide, winStroke.getX(), winStroke.getY());
            return true;
        }
        else {
            Stroke blockStroke = getBlockStroke();
            if (blockStroke != null) {  // если могу блокировать
                setMark(computerSide, blockStroke.getX(), blockStroke.getY());
                return false;
            } else return computerTurnEasyLevel();
        }
    }
}

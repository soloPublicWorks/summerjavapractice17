package javaLevel1.lesson2;

import java.util.Arrays;

/**
 * Created by Валера on 30.06.2017.
 */
public class Main {
    public static void main(String[] args) {
        // task1
        int[] arr = {1, 0, 0, 1, 1, 1, 0};
        for (int i = 0; i < arr.length; i++)
            if (arr[i] == 0) arr[i] = 1;
            else if (arr[i] == 1) arr[i] = 0;
        System.out.println(Arrays.toString(arr) + "\n");

        // task2
        int[] arr3 = new int[8];
        for (int i = 1; i < arr3.length; i++)
            arr3[i] = arr3[i - 1] + 3;
        System.out.println(Arrays.toString(arr3) + "\n");

        // task3
        int[] mas = { 1, 5, 3, 2, 11, 4, 5, 2, 4, 8, 9, 1 };
        for (int i = 0; i < mas.length; i++)
            if (mas[i] < 6) mas[i] *= 2;
        System.out.println(Arrays.toString(mas) + "\n");

        // task4
        int[][] arrTask4 = new int[10][10];
        for (int i = 0; i < arrTask4.length; i++)
            for (int j = 0; j < arrTask4.length; j++)
                if (i == j || arrTask4.length - 1 - i == j) arrTask4[i][j] = 1;
        for (int[] anArrTask4 : arrTask4) System.out.println(Arrays.toString(anArrTask4));
        System.out.println();

        // task5
        int max = mas[0], min = mas[0];
        for (int i = 1; i < mas.length; i++) {
            if (mas[i] > max) max = mas[i];
            if (mas[i] < min) min = mas[i];
        }
        System.out.println(min + " | " + max + "\n");

        // task6
        System.out.println(checkBalance(new int[]{1, 1, 1, 2, 1}) + "\n");
        System.out.println(checkBalance(new int[] {2, 1, 1, 2, 1}) + "\n");
        System.out.println(checkBalance(new int[] {10, 10}) + "\n");

        // task7
        int[] array = {0, 1, 2, 3, 4, 5, 6};
        System.out.println(Arrays.toString(array) + "\n");
        System.out.println("Сдвиг на " + 0 + " позиций: " + Arrays.toString(shiftByN(array, 0)));
        array = new int[]{0, 1, 2, 3, 4, 5, 6};
        System.out.println("Сдвиг на " + 1 + " позицию: " + Arrays.toString(shiftByN(array, 1)));
        array = new int[]{0, 1, 2, 3, 4, 5, 6};
        System.out.println("Сдвиг на " + -1 + " позицию: " + Arrays.toString(shiftByN(array, -1)));
        array = new int[]{0, 1, 2, 3, 4, 5, 6};
        System.out.println("Сдвиг на " + 3 + " позиции: " + Arrays.toString(shiftByN(array, 3)));
        array = new int[]{0, 1, 2, 3, 4, 5, 6};
        System.out.println("Сдвиг на " + -3 + " позиции: " + Arrays.toString(shiftByN(array, -3)));
        array = new int[]{0, 1, 2, 3, 4, 5, 6};
        System.out.println("Сдвиг на " + 10 + " позиций: " + Arrays.toString(shiftByN(array, 10)));
        array = new int[]{0, 1, 2, 3, 4, 5, 6};
        System.out.println("Сдвиг на " + -10 + " позиций: " + Arrays.toString(shiftByN(array, -10)));
    }
    private static int[] shiftByN(int[] arr, int n) {
        n = n % arr.length;
        if (n > 0) n = arr.length - n;
        else if (n < 0) n = Math.abs(n);
        else return arr;
        arr = reverseArray(arr, 0, n - 1);
        arr = reverseArray(arr, n, arr.length - 1);
        return reverseArray(arr, 0, arr.length - 1);
    }
    private static int[] reverseArray(int[] arr, int from, int to) {
        if (from <= to) {
            int tmp;
            for (int i = 0; i <= (to - from) / 2; i++) {
                tmp = arr[from + i];
                arr[from + i] = arr[to - i];
                arr[to - i] = tmp;
            }
            return arr;
        } else throw new IllegalArgumentException("Индекс левого конца не должен быть больше индекса правого");
    }
    private static boolean checkBalance(int[] arr) {
        int leftSum = 0, rightSum;
        for (int i = 0; i < arr.length - 1; i++) {
            leftSum += arr[i];
            rightSum = 0;
            for (int j = i + 1; j < arr.length; j++)
                rightSum += arr[j];
            if (leftSum == rightSum) return true;
        }
        return false;
    }
}

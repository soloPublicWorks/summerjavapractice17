package javaLevel1.lesson3;

import java.util.Scanner;

/**
 * Created by Валера on 04.07.2017.
 */
public class Task2 {
    public static final int MISMATCHES_NUMBER = 15;
    public static final char MISMATCH = '#';
    public static void main(String[] args) {
        String[] words = {"apple", "orange", "lemon", "banana", "apricot", "avocado", "broccoli", "carrot",
                "cherry", "garlic", "grape", "melon", "leak", "kiwi", "mango", "mushroom", "nut", "olive", "pea",
                "peanut", "pear", "pepper", "pineapple", "pumpkin", "potato"};
        play(words);
    }
    public static void play(String[] words) {
        System.out.println("Попытайтесь отгадать загаданное компьютером число.");
        Scanner scanner = new Scanner(System.in);
        String requiredWord = words[(int) (Math.random()*words.length)];
        while (!guessWord(requiredWord, scanner)) {}
    }
    private static boolean guessWord(String requiredWord, Scanner scanner) {
        System.out.print("Введите слово: ");
        String currentWord = scanner.nextLine();
        if (currentWord.equals(requiredWord)) {
            System.out.println("Вы угадали слово!");
            return true;
        } else {
            System.out.println("Вы не угадали слово. Схожие буквы: ");
            compareWords(currentWord, requiredWord);
            return false;
        }
    }
    private static void compareWords(String currentWord, String requiredWord) {
        String samePart = "";
        for (int i = 0; i < Math.min(currentWord.length(), requiredWord.length()); i++) {
            if (currentWord.charAt(i) == requiredWord.charAt(i)) samePart += currentWord.charAt(i);
            else samePart += "#";
        }
        System.out.println(addMismatches(samePart));
    }
    private static String addMismatches(String samePart) {
        for (int i = 0; i < MISMATCHES_NUMBER; i++) samePart += MISMATCH;
        return samePart;
    }
}

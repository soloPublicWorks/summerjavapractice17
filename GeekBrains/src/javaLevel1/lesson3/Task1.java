package javaLevel1.lesson3;

import java.util.Scanner;

/**
 * Created by Валера on 04.07.2017.
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        byte requiredNumber;    boolean guessingResult;
        do {
            introduction();
            requiredNumber = (byte) (Math.random()*9);
            guessingResult = guessNumber(requiredNumber, scanner);
        } while (wantRepeat(guessingResult, requiredNumber, scanner));
    }
    private static void introduction() {
        System.out.println("Загадано случайное число от 0 до 9.\n" +
                "У Вас есть 3 попытки, для того чтобы угадать его.");
        printBorder();
    }
    private static boolean guessNumber(byte requiredNumber, Scanner scanner) {
        byte attemptCount = 3;
        boolean numberGuessed = false;
        byte currentNumber;
        do {
            System.out.print("Осталось попыток: " + attemptCount + "\nВведите число: ");
            if (scanner.hasNextByte()) {
                currentNumber = scanner.nextByte();
                numberGuessed = compareCurrentWithRequired(currentNumber, requiredNumber);
            } else { System.out.println("Неверный ввод!"); scanner.nextLine(); }
            attemptCount--;
        } while (attemptCount != 0 && !numberGuessed);
        printBorder();
        return numberGuessed;
    }
    private static boolean compareCurrentWithRequired(byte currentNumber, byte requiredNumber) {
        boolean areEqual = false;
        if (currentNumber > requiredNumber) System.out.println("Введенное число больше искомого.");
        else if (currentNumber < requiredNumber) System.out.println("Введенное число меньше искомого.");
        else areEqual = true;
        return areEqual;
    }
    private static boolean wantRepeat(boolean guessingResult, byte requiredNumber, Scanner scanner) {
        boolean correctInput = false;
        byte answer = 0;
        if (guessingResult) System.out.println("Поздравляю! Вы угадали!");
        else System.out.println("К сожалению вы не справились... Искомое число: " + requiredNumber);
        System.out.println("Повторить игру еще раз? 1 – да / 0 – нет");
        do {
            System.out.print("Ваш выбор: ");
            if (scanner.hasNextByte())  {
                answer = scanner.nextByte();
                if (answer == 0 || answer == 1) correctInput = true;
            }
            else { System.out.println("Неверный ввод!"); scanner.nextLine(); }
        } while (!correctInput);
        printBorder();
        return (answer == 1) ? true : false;
    }

    private static void printBorder() {
        System.out.println("-----------------------------------------");
    }
}

package javaLevel1.lesson1.extra;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Валера on 30.06.2017.
 */
public class ConsoleCalculator {
    private Number number1;
    private Number number2;
    private Character operation;

    public void inputData(Scanner scanner) {
        boolean unsuccessfulInputNumbers = true,
                unsuccessfulInputOperation = true;
        while (unsuccessfulInputNumbers) {
            try {
                System.out.println("Введите два числа: ");
                getNumbers(scanner);
                unsuccessfulInputNumbers = false;
            } catch (Exception e) {
                System.out.println(e.getMessage());
                scanner.nextLine();
            }
        }
        while (unsuccessfulInputOperation) {
            try {
                System.out.println("Введите операцию: ");
                getOperation(scanner);
                unsuccessfulInputOperation = false;
            } catch (Exception e) {
                System.out.println(e.getMessage());
                scanner.nextLine();
            }
        }
    }
    public void calculate() {
        Number result = null;
        switch (operation) {
            case '+':
                result = number1.doubleValue() + number2.doubleValue();
                break;
            case '-':
                result = number1.doubleValue() - number2.doubleValue();
                break;
            case '*':
                result = number1.doubleValue() * number2.doubleValue();
                break;
            case '/':
                result = number1.doubleValue() / number2.doubleValue();
                break;
        }
        System.out.println("Результат: " + result.toString());
    }

    private void getNumbers(Scanner scanner) throws InputMismatchException {
        String number1String, number2String;
        try {
            number1String = scanner.next("[0-9]*");
            number2String = scanner.next("[0-9]*");
            number1 = Double.parseDouble(number1String);
            number2 = Double.parseDouble(number2String);
        } catch (InputMismatchException e) {
            throw new InputMismatchException("Неверный ввод числа!");
        }
    }
    private void getOperation(Scanner scanner) throws IllegalArgumentException {
        try {
            String operationString = scanner.next("[+,-,*,/]");
            operation = operationString.charAt(0);
        } catch (InputMismatchException e) {
            throw new InputMismatchException("Неверный ввод операции");
        }
    }
}


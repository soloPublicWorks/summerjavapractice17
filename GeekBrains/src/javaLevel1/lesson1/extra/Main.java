package javaLevel1.lesson1.extra;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Валера on 30.06.2017.
 */
public class Main {
    public static void main(String[] args) {
        //task1
        int[] arr = new int[] {1, 0, 0, 1};

        // task2
        for (int i = 0; i < arr.length; i++)
            if (arr[i] == 0) arr[i] = 1;
            else if (arr[i] == 1) arr[i] = 0;
        System.out.println(Arrays.toString(arr));

        // task3
        int[] arr3 = new int[8];
        for (int i = 0; i < arr3.length; i++)
            if (i == 0) arr3[i] = 1;
            else arr3[i] = arr3[i - 1] + 3;
        System.out.println(Arrays.toString(arr3));

        // task4
        int[] mas = { 1, 5, 3, 2, 11, 4, 5, 2, 4, 8, 9, 1 };
        for (int i = 0; i < mas.length; i++)
            if (mas[i] < 6) mas[i] *= 2;
        System.out.println(Arrays.toString(mas));

        // task5
        int max = mas[0], min = mas[0];
        for (int i = 1; i < mas.length; i++) {
            if (mas[i] > max) max = mas[i];
            if (mas[i] < min) min = mas[i];
        }
        System.out.println(min + " | " + max);

        // task6
        Scanner scanner = new Scanner(System.in);
        ConsoleCalculator consoleCalculator = new ConsoleCalculator();
        consoleCalculator.inputData(scanner);
        consoleCalculator.calculate();
    }
}

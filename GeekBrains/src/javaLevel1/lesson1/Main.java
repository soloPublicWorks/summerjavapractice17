package javaLevel1.lesson1;

/**
 * Created by Валера on 30.06.2017.
 */
public class Main {
    // task1
    public static void main(String[] args) {
        // task2
        double a = 1, b = 2, c = 3, d = 4;  // 8 байт. [-4.9e-324; 1.7e+308]
        float f = 1.23f;                    // 4 байт. [-1.4e-45f; 3.4e+38f]
        byte b1 = 1, b2 = 2;                // 1 байт. [-128; 127]
        short sh = 10;                      // 2 байт. [-32768; 32767]
        char c1 = 'a';                      // 2 байт. символ UTF-16
        int year = 2000;                    // 4 байт. [-32768; 32767]
        long l = 4L;                        // 8 байт. [-9223372036854775808; 9223372036854775807]
        boolean bool = true;                // 4 байт. [true, false]. default = false
        String str = "str";

        // task3
        System.out.println(a + " * (" + b + " + (" + c + " / " + d + ")) = " + methodTask3(a, b, c, d));

        // task4
        System.out.println("10 <= (" + b1 +" + " + b2 + ") <= 20 : " + methodTask4(b1, b2));
        b1 = 5; b2 = 10;
        System.out.println("10 <= (" + b1 +" + " + b2 + ") <= 20 : " + methodTask4(b1, b2));

        // task5
        System.out.println(year + " is leap: " + isLeapYear(year));
        year = 1900;
        System.out.println(year + " is leap: " + isLeapYear(year));
        year = 1804;
        System.out.println(year + " is leap: " + isLeapYear(year));
    }
    private static double methodTask3(double a, double b, double c, double d) {
        return a*(b + (c / d));
    }
    private static boolean methodTask4(byte b, byte b1) {
        return (b + b1 >= 10 && b + b1 <= 20) ? true : false;
    }
    private static boolean isLeapYear(int year) {
        return (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
    }
}

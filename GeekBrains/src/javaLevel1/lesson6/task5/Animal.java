package javaLevel1.lesson6.task5;

/**
 * Created by Валера on 15.07.2017.
 */
public abstract class Animal {
    protected String name;

    public abstract void run(double obstacleLength);
    public abstract void swim(double obstacleLength);
    public abstract void jump(double obstacleHeight);
}

package javaLevel1.lesson6.task5.dog;

/**
 * Created by Валера on 15.07.2017.
 */
public class Husky extends Dog {
    private static final double MAX_RUNNING_LENGTH = 10000;
    private static final double MAX_SWIMMING_LENGTH = 50;

    private boolean isSled;

    public Husky(String name, String speciality, boolean isSled) {
        super(name, speciality);
        this.isSled = isSled;
    }
    @Override
    public void swim(double obstacleLength) {
        System.out.println(this
                + "Can swim: " + obstacleLength + " : " +
                ((obstacleLength >= 0) && (obstacleLength <= MAX_SWIMMING_LENGTH)));
    }
    @Override
    public void run(double obstacleLength) {
        System.out.println(this +
                "Can run " + obstacleLength + " : " +
                ((obstacleLength >= 0) && (obstacleLength <= MAX_RUNNING_LENGTH)));
    }

    @Override
    public String toString() {
        return "Husky " + "\n" +
                "Name: " + super.name + "\n" +
                "Speciality: " + super.speciality + "\n" +
                "isSled: " + isSled + "\n";
    }
}

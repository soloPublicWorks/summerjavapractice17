package javaLevel1.lesson6.task5.dog;

import javaLevel1.lesson6.task5.Animal;

/**
 * Created by Валера on 15.07.2017.
 */
public class Dog extends Animal {
    private static final double MAX_RUNNING_LENGTH = 50;
    private static final double MAX_SWIMMING_LENGTH = 0.5;
    private static final double MAX_JUMPING_HEIGHT = 2;

    protected String speciality;

    public Dog(String name, String speciality) {
        super.name = name;
        this.speciality = speciality;
    }

    @Override
    public void run(double obstacleLength) {
        System.out.println(this +
                "Can run " + obstacleLength + " : " +
                ((obstacleLength >= 0) && (obstacleLength <= MAX_RUNNING_LENGTH)));
    }
    @Override
    public void swim(double obstacleLength) {
        System.out.println(this
                + "Can swim: " + obstacleLength + " : " +
                ((obstacleLength >= 0) && (obstacleLength <= MAX_SWIMMING_LENGTH)));
    }
    @Override
    public void jump(double obstacleHeight) {
        System.out.println(this +
                "Can jump: " + obstacleHeight + " : " +
                ((obstacleHeight >= 0) && (obstacleHeight <= MAX_JUMPING_HEIGHT)));
    }

    @Override
    public String toString() {
        return "Dog " + "\n" +
                "Name: " + super.name + "\n" +
                "Speciality: " + speciality + "\n";
    }
}

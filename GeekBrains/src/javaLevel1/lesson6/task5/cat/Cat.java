package javaLevel1.lesson6.task5.cat;

import javaLevel1.lesson6.task5.Animal;

/**
 * Created by Валера on 15.07.2017.
 */
public class Cat extends Animal {
    private static final double MAX_RUNNING_LENGTH = 50;
    private static final double MAX_SWIMMING_LENGTH = 0.5;
    private static final double MAX_JUMPING_HEIGHT = 2;

    protected boolean valuesPeople;

    public Cat(String name, boolean valuesPeople) {
        this.name = name;
        this.valuesPeople = valuesPeople;
    }

    @Override
    public void run(double obstacleLength) {
        System.out.println(this +
                "Can run " + obstacleLength + " : " +
                ((obstacleLength >= 0) && (obstacleLength <= MAX_RUNNING_LENGTH)));
    }
    @Override
    public void swim(double obstacleLength) {
        System.out.println(this
                + "Can swim: " + obstacleLength + " : " +
                ((obstacleLength >= 0) && (obstacleLength <= MAX_SWIMMING_LENGTH)));
    }
    @Override
    public void jump(double obstacleHeight) {
        System.out.println(this +
                "Can jump: " + obstacleHeight + " : " +
                ((obstacleHeight >= 0) && (obstacleHeight <= MAX_JUMPING_HEIGHT)));
    }

    @Override
    public String toString() {
        return "Cat " + "\n" +
                "Name: " + this.name + "\n" +
                "ValuesPeople: " + valuesPeople + "\n";
    }
}

package javaLevel1.lesson6.task5.cat;

/**
 * Created by Валера on 15.07.2017.
 */
public class EgyptianMau extends Cat {
    private static final double MAX_RUNNING_LENGTH = 40;

    private boolean isBrindle;

    public EgyptianMau(String name, boolean valuesPeople, boolean isBrindle) {
        super(name, valuesPeople);
        this.isBrindle = isBrindle;
    }

    @Override
    public void run(double obstacleLength) {
        System.out.println(this +
                "Can run " + obstacleLength + " : " +
                ((obstacleLength >= 0) && (obstacleLength <= MAX_RUNNING_LENGTH)));
    }

    @Override
    public String toString() {
        return "EgyptianMau " + "\n" +
                "Name: " + this.name + "\n" +
                "ValuesPeople: " + this.valuesPeople + "\n" +
                "isBrindle: " + isBrindle + "\n";
    }
}

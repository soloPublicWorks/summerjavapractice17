package javaLevel1.lesson6.task5;

import javaLevel1.lesson6.task5.cat.Cat;
import javaLevel1.lesson6.task5.cat.EgyptianMau;

/**
 * Created by Валера on 15.07.2017.
 */
public class Main {
    public static void main(String[] args) {
        Animal mau = new EgyptianMau("Mau", false, true);
        Animal cat = new Cat("Cat", false);

        mau.run(39);
        System.out.println();
        mau.swim(100);
        System.out.println();
        cat.run(49);
        System.out.println();
        cat.swim(0.2);
    }
}

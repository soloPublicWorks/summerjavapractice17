package javaLevel1.lesson6.task1234;

/**
 * Created by Валера on 15.07.2017.
 */
public class Main {
    public static void main(String[] args) {
        Animal cat = new Cat();
        Animal dog = new Dog();

        cat.jump(2);
        cat.run(10);
        cat.swim(10);

        dog.jump(1);
        dog.run(10);
        dog.swim(10);
    }
}

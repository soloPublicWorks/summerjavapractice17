package javaLevel1.lesson6.task1234;

/**
 * Created by Валера on 15.07.2017.
 */
public abstract class Animal {
    public abstract void run(double obstacleLength);
    public abstract void swim(double obstacleLength);
    public abstract void jump(double obstacleHeight);
}

package javaLevel1.lesson6.task1234;

/**
 * Created by Валера on 15.07.2017.
 5. * Добавить животным разброс в ограничениях. То есть у одной собаки ограничение на бег может быть 400 м., у другой 600 м.
 */
public class Cat extends Animal {
    private static final double MAX_RUNNING_LENGTH = 200;
    private static final double MAX_JUMPING_HEIGHT = 2;
    private static final double MAX_SWIMMING_LENGTH = 0;

    @Override
    public void run(double obstacleLength) {
        System.out.println("Run: " + ((obstacleLength >= 0) && (obstacleLength <= MAX_RUNNING_LENGTH)));
    }
    @Override
    public void swim(double obstacleLength) {
        System.out.println("Swim: " + ((obstacleLength >= 0) && (obstacleLength <= MAX_SWIMMING_LENGTH)));
    }
    @Override
    public void jump(double obstacleHeight) {
        System.out.println("Jump: " + ((obstacleHeight >= 0) && (obstacleHeight <= MAX_JUMPING_HEIGHT)));
    }
}

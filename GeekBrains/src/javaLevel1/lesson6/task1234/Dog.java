package javaLevel1.lesson6.task1234;

/**
 * Created by Валера on 15.07.2017.
 */
public class Dog extends Animal {
    private static final double MAX_RUNNING_LENGTH = 500;
    private static final double MAX_JUMPING_HEIGHT = 0.5;
    private static final double MAX_SWIMMING_LENGTH = 10;

    @Override
    public void run(double obstacleLength) {
        System.out.println("Run: " + ((obstacleLength >= 0) && (obstacleLength <= MAX_RUNNING_LENGTH)));
    }
    @Override
    public void swim(double obstacleLength) {
        System.out.println("Swim: " + ((obstacleLength >= 0) && (obstacleLength <= MAX_SWIMMING_LENGTH)));
    }
    @Override
    public void jump(double obstacleHeight) {
        System.out.println("Jump: " + ((obstacleHeight >= 0) && (obstacleHeight <= MAX_JUMPING_HEIGHT)));
    }
}

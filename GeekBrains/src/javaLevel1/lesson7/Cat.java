package javaLevel1.lesson7;

/**
 * Created by Валера on 20.07.2017.
 */
public class Cat {
    private String name;
    private int appetite;
    private boolean isSatiety;

    public Cat(String name, int appetite) {
        this.name = name;
        this.appetite = appetite;
        isSatiety = false;
    }

    public void eat(Plate plate) {
        // если достаточно еды, чтобы насесться
        if (plate.decreaseFood(appetite)) isSatiety = true;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", appetite=" + appetite +
                ", isSatiety=" + isSatiety +
                '}';
    }
}

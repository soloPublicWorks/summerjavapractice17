package javaLevel1.lesson7;

/**
 * Created by Валера on 20.07.2017.
 */
public class Plate {
    private int capacity;
    private int food;

    public Plate(int capacity, int food) {
        this.capacity = capacity;
        this.food = food;
    }

    public boolean decreaseFood(int appetite) { // возвращает индикатор успешного понижения
        if (food - appetite >= 0)  {
            food -= appetite;
            return true;
        } else return false;
    }

    public void addFood(int food) {
        if (this.food + food > capacity) this.food = capacity;   // если корма слишком много, то он высыпается из тарелки
        else this.food += food;
    }

    @Override
    public String toString() {
        return "Plate{" +
                "capacity=" + capacity +
                ", food=" + food +
                '}';
    }
}

package javaLevel1.lesson7;

/**
 * Created by Валера on 20.07.2017.
 */
public class Main {
    public static final int NUMBER_CAT = 4;

    public static void main(String[] args) {
        Cat[] cats = new Cat[NUMBER_CAT];
        for (int i = 0; i < NUMBER_CAT; i++)
            cats[i] = new Cat("Vasya " + i, (int)(Math.random()*60));
        Plate plate = new Plate(100, 100);

        // кушаем
        for (Cat cat : cats) {
                System.out.println("Кот поел: ");
            cat.eat(plate);
                System.out.println(cat);
                System.out.println(plate);
                System.out.println();
                System.out.println("Добавили корм: ");
            plate.addFood(((int) (Math.random() * 5))); // после каждого подсыпываем корм
                System.out.println(plate);
                System.out.println();
        }

        // состояние котов и тарелки после еды
        System.out.println();
        System.out.println("Итог: ");
        for (Cat cat : cats)
            System.out.println(cat);
        System.out.println(plate);
    }
}

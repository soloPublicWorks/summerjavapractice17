package javaLevel1.lesson8;

import javafx.scene.layout.Border;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by Валера on 21.07.2017.
 */
class MyWindow extends JFrame {
    public void initMyWindowBoredLayout1(JFrame window) {
        window.setTitle("Test window");
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);    // завершение работы при закрытии окна
        window.setBounds(300, 300, 400, 400);
        window.setLayout(new BorderLayout());  // установка макета расположения элементов на окне

        JButton[] jButtons = new JButton[5];
        for (int i = 0; i < jButtons.length; i++)
            jButtons[i] = new JButton("JButton №" + i);
        // добавление элементов с указанием их расположения на макете
        window.add(jButtons[0], BorderLayout.NORTH);
        window.add(jButtons[1], BorderLayout.EAST);
        window.add(jButtons[2], BorderLayout.SOUTH);
        window.add(jButtons[3], BorderLayout.WEST);
        window.add(jButtons[4], BorderLayout.CENTER);

        window.setVisible(true);
    }
    public void initMyWindowBoredLayout2(JFrame window) {
        window.setTitle("Test window");
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);    // завершение работы при закрытии окна
        window.setBounds(300, 300, 400, 400);

        JButton jButton = new JButton("Button №1(PAGE_START)");
        window.add(jButton, BorderLayout.PAGE_START);

        jButton = new JButton("Button №2(CENTER)");
        jButton.setPreferredSize(new Dimension(200, 100));
        window.add(jButton, BorderLayout.CENTER);

        jButton = new JButton("Button №3(LINE_START)");
        window.add(jButton, BorderLayout.LINE_START);

        jButton = new JButton("Long-named Button №4(PAGE_END)");
        window.add(jButton, BorderLayout.PAGE_END);

        jButton = new JButton("Button №5(LINE_END)");
        window.add(jButton, BorderLayout.LINE_END);

        window.setVisible(true);
    }
    public void initMyWindowBoxLayout(JFrame window) {
        window.setTitle("Test window");
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);    // завершение работы при закрытии окна
        window.setBounds(500, 500, 500, 300);
        //window.setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));    // расположение в строку
        window.setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));    // расположение в столбец
        //window.setResizable(false); // запретить изменение размера окна

        JButton[] jbuttons = new JButton[4];
        for (int i = 0; i < jbuttons.length; i++) {
            jbuttons[i] = new JButton("Button №" + i);
            jbuttons[i].setAlignmentX(CENTER_ALIGNMENT);    // выравнивание по центру
            window.add(jbuttons[i]);
        }

        window.setVisible(true);
    }
    public void initMyWindowFlowLayout(JFrame window) {
        window.setTitle("Test window");
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);    // завершение работы при закрытии окна
        window.setBounds(500, 500, 400, 300);
        window.setLayout(new FlowLayout());    // расположение в строку, при преувеличении ширина окна - перенос на следующую строку

        JButton[] jbuttons = new JButton[10];
        for (int i = 0; i < jbuttons.length; i++) {
            jbuttons[i] = new JButton("Button №" + i);
            window.add(jbuttons[i]);
        }

        window.setVisible(true);
    }
    public void initMyWindowGridLayout(JFrame window) {
        window.setTitle("Test window");
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);    // завершение работы при закрытии окна
        window.setBounds(500, 500, 400, 300);
        window.setLayout(new GridLayout(4, 3)); // табличное расположение элементов

        JButton[] jbuttons = new JButton[10];
        for (int i = 0; i < jbuttons.length; i++) {
            jbuttons[i] = new JButton("Button №" + i);
            window.add(jbuttons[i]);
        }

        window.setVisible(true);
    }
    public void initMyWindowWithClickableButton(JFrame window) {
        window.setTitle("Test window");
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);    // завершение работы при закрытии окна
        window.setBounds(500, 500, 400, 300);
        window.setLayout(new GridLayout(4, 3)); // табличное расположение элементов

        JButton jButton = new JButton("Action button");
        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Button pressed");
            }
        });
        window.add(jButton);

        window.setVisible(true);
    }
    public void initMyWindowWithClickableEnterKey(JFrame window) {
        window.setTitle("Test window");
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);    // завершение работы при закрытии окна
        window.setBounds(500, 500, 400, 300);
        window.setLayout(new FlowLayout());

        JTextField jTextField = new JTextField("JTextField");
        jTextField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Text from JTextField: " + jTextField.getText());
            }
        });
        window.add(jTextField);

        window.setVisible(true);
    }
    public void initMyWindowWithCatchClickPlace(JFrame window) {
        window.setTitle("Test window");
        window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);    // завершение работы при закрытии окна
        window.setBounds(500, 500, 400, 300);

        JPanel jPanel = new JPanel();
        jPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                System.out.println("Mouse position: (" + e.getX() + " ; " + e.getY() + ")");
            }
        });
        window.add(jPanel);

        window.setVisible(true);
    }

    public MyWindow() throws HeadlessException {
        //initMyWindowBoredLayout1(this);
        //initMyWindowBoredLayout2(this);
        //initMyWindowBoxLayout(this);
        //initMyWindowFlowLayout(this);
        //initMyWindowGridLayout(this);
        //initMyWindowWithClickableButton(this);
        //initMyWindowWithClickableEnterKey(this);
        initMyWindowWithCatchClickPlace(this);
    }
}

class Main {
    public static void main(String[] args) {
        MyWindow myWindow = new MyWindow();
    }
}

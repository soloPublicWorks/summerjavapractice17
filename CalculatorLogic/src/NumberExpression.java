/**
 * Created by Валера on 27.08.2017.
 *
 * терминальное выражение - единица выражения
 *
 */
public class NumberExpression implements Expression {
    private double number;

    public NumberExpression(double number) {
        this.number = number;
    }

    @Override
    public double interpret() {
        return number;
    }
}

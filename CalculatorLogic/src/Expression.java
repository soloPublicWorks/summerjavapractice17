/**
 * Created by Валера on 27.08.2017.
 */
public interface Expression {
    double interpret();
}

/**
 * Created by Валера on 27.08.2017.
 *
 * нетерминальное выражение - разлагаемое
 *
 */
public class SubExpression extends OperationExpression {
    public SubExpression(Expression expressionLeft, Expression expressionRight) {
        super(expressionLeft, expressionRight);
    }

    @Override
    public double interpret() {
        return expressionLeft.interpret() - expressionRight.interpret();
    }
}

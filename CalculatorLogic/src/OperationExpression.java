/**
 * Created by Валера on 27.08.2017.
 */
public abstract class OperationExpression implements Expression{
    protected Expression expressionLeft;
    protected Expression expressionRight;

    public OperationExpression(Expression expressionLeft, Expression expressionRight) {
        this.expressionLeft = expressionLeft;
        this.expressionRight = expressionRight;
    }

    @Override
    public abstract double interpret();
}

/**
 * Created by Валера on 27.08.2017.
 */
public class AddExpression extends OperationExpression{
    public AddExpression(Expression expressionLeft, Expression expressionRight) {
        super(expressionLeft, expressionRight);
    }

    @Override
    public double interpret() {
        return expressionLeft.interpret() + expressionRight.interpret();
    }
}
